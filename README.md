# VER'TIGE

Détail de l'application

## Installation

Premièrement, la base de données se génère automatiquement au lancement du back et se met a jour en conséquence.

Ensuite, s'il s'agit d'un lancement en local, il suffit juste de démarrer le projet en tant que SpringBoot Application et d'avoir au préalable XAMPP de lancé.
Sinon, il sera nécessaire de déployer le projet sur un serveur.

Par défaut, l'application se lance sur le port 8088.

## Détails des modèles

Voici les modèles disponibles à l'heure actuelle:

	- Photo : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| name | String | |
| url | String | |

	- Category : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| name | String | |
| description | String | |

	- Family : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| name | String | |
| description | String | |
| id_category | Category | |

	- Soil : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| name | String | |
| description | String | |

	- Weather : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| name | String | |
| description | String | |

	- Period : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| name | String | |
| start_date | String | |
| end_date | String | |

	- Pest : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| name | String | |
| description | String | |
| treatment | String | |
| isDisease | Boolean | |
| photo | Photo | |

	- Plant : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| name | String | |
| description | String | |
| photos | String | |
| sunshine | Double | |
| watering | Double | |
| width | Double | |
| height | Double | |
| distance | Double | |
| frost_resistance | Double | |
| hygrometry | Double | |
| isEdible | Boolean | |
| taste_quality | int | |
| virtues | String | |
| avoid | Set{Plant} | |
| cohabit | Set{Plant} | |
| family | Family | NotNull / NotEmpty |
| soils | Set{Soil} | |
| weathers | Set{Weather} | |
| pests | Set{Pest} | |
| trim | Set{Period} | |
| sow | Set{Period} | |
| fertilization | Set{Period} | |
| harvest | Set{Period} | |
| plantation | Set{Period} | |

	- User : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| pseudo | String | |
| mail | String | |
| password | String | |
| city | String | |
| postal code| String | |
| favorites | Set{Plant} | |

	- Garden : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| date_create | Date | |
| user | User | |
| parcels | Set{Parcel} | |
| rows | int | |
| cols | int | |

	- Parcel : 
| Nom | Type | Detail |
| --- | --- | --- |
| id | Long | |
| name | String | |
| num_row | int | |
| num_column | int | |
| plant | Plant | |

## Arborescence des routes

Voici les routes disponibles à l'heure actuelle ainsi que le type de ressources demandées:

| Route | Detail |
| --- | --- |
| PHOTO |
| /api/photo | (GET) retourne la liste de toutes les photos |
| /api/photo/add-photo | (POST) permet dajouter une photo en se servant du modèle Photo |
| /api/photo/findId/{id} | (GET) retourne la photo correspondant à l'id indiqué (si existant) |
| /api/photo/edit/{id} | (PUT) permet de modifier les informations de la photo |
| /api/photo/remove/{id} | (DELETE) permet de supprimer la photo correspondant à l'id indiqué |
| --- | --- |
| CATEGORY |
| /api/category | (GET) retourne la liste de toutes les catégories |
| /api/category | (POST) permet dajouter une catégorie en se servant du modèle Category |
| /api/category/findId/{id} | (GET) retourne la catégorie correspondant à l'id indiqué (si existant) |
| /api/category/edit/{id} | (PUT) permet de modifier les informations de la catégorie |
| /api/category/remove/{id} | (DELETE) permet de supprimer la catégorie correspondant à l'id indiqué |
| --- | --- |
| FAMILY |
| /api/family | (GET) retourne la liste de toutes les familles |
| /api/family | (POST) permet dajouter une famille en se servant du modèle Family |
| /api/family/findId/{id} | (GET) retourne la famille correspondant à l'id indiqué (si existant) |
| /api/family/edit/{id} | (PUT) permet de modifier les informations de la famille |
| /api/family/remove/{id} | (DELETE) permet de supprimer la famille correspondant à l'id indiqué |
| --- | --- |
| SOIL |
| /api/soil | (GET) retourne la liste de tous les sols |
| /api/soil | (POST) permet dajouter un sol en se servant du modèle Soil |
| /api/soil/findId/{id} | (GET) retourne le sol correspondant à l'id indiqué (si existant) |
| /api/soil/edit/{id} | (PUT) permet de modifier les informations du sol |
| /api/soil/remove/{id} | (DELETE) permet de supprimer le sol correspondant à l'id indiqué |
| --- | --- |
| WEATHER |
| /api/weather | (GET) retourne la liste de tous les climats |
| /api/weather | (POST) permet dajouter un climat en se servant du modèle Weather |
| /api/weather/findId/{id} | (GET) retourne lu climat correspondant à l'id indiqué (si existant) |
| /api/weather/edit/{id} | (PUT) permet de modifier les informations du climat |
| /api/weather/remove/{id} | (DELETE) permet de supprimer le climat correspondant à l'id indiqué |
| --- | --- |
| PERIOD |
| /api/period | (GET) retourne la liste de toutes les périodes |
| /api/period | (POST) permet dajouter une période en se servant du modèle Period |
| /api/period/findId/{id} | (GET) retourne la période correspondant à l'id indiqué (si existant) |
| /api/period/edit/{id} | (PUT) permet de modifier les informations de la période |
| /api/period/remove/{id} | (DELETE) permet de supprimer la période correspondant à l'id indiqué |
| --- | --- |
| PEST |
| /api/pest | (GET) retourne la liste de toutes les pestes |
| /api/pest | (POST) permet dajouter une peste en se servant du modèle Pest |
| /api/pest/findId/{id} | (GET) retourne la peste correspondant à l'id indiqué (si existant) |
| /api/pest/edit/{id} | (PUT) permet de modifier les informations de la peste |
| /api/pest/remove/{id} | (DELETE) permet de supprimer la peste correspondant à l'id indiqué |
| --- | --- |
| PLANT |
| /api/plant | (GET) retourne la liste de toutes les plantes |
| /api/plant | (POST) permet dajouter une plante en se servant du modèle Plant |
| /api/plant/findId/{id} | (GET) retourne la plante correspondant à l'id indiqué (si existant) |
| /api/plant/find-name/{name} | (GET) retourne la plante correspondant au nom indiqué (si existant) |
| /api/plant/edit/{id} | (PUT) permet de modifier les informations de la plante |
| /api/plant/remove/{id} | (DELETE) permet de supprimer la plante correspondant à l'id indiqué |
| --- | --- |
| USER |
| /api/user | (GET) retourne la liste de tous les utilisateurs |
| /api/user/register | (POST) permet d'inscrire un utilisateur en se servant du modèle User |
| /api/user/login | (POST) permet de vérifier et d'authentifier l'utilisateur en se servant d'un array de 2 String (pseudo, password) |
| /api/user/findId/{id} | (GET) retourne l'utilisateur correspondant à l'id indiqué (s'il existe) |
| /api/user/findPseudo/{pseudo} | (GET) retourne l'utilisateur correspondant au pseudo indiqué (s'il existe) |
| /api/user/edit/{id} | (PUT) permet de modifier les informations de l'utilisateur (non fonctionnelle pour le moment) |
| /api/user/remove/{id} | (DELETE) permet de supprimer l'utilisateur correspondant à l'id indiqué |
| /api/user/favs/{idUser}/{idPlant} | (PUT) permet d'ajouter/supprimer la plante dans la liste des favoris de l'utilisateur |
| --- | --- |
| GARDEN |
| /api/garden | (GET) retourne la liste de tous les jardins |
| /api/garden/add-garden | (POST) permet d'ajouter un jardin en se servant du modèle Garden |
| /api/garden/update-garden | (PUT) permet de modifier le jardin |
| /api/garden/findId/{id} | (GET) retourne le jardin correspondant à l'id indiqué |
| --- | --- |
| GARDEN |
| /api/garden | (GET) retourne la liste de tous les jardins |
| /api/garden/add-garden | (POST) permet d'ajouter un jardin en se servant du modèle Garden |
| /api/garden/update-garden | (PUT) permet de modifier le jardin |
| /api/garden/findId/{id} | (GET) retourne le jardin correspondant à l'id indiqué |

---

		
	- /api/plant : retourne la liste de toutes les plantes
		- /add-plant : permet d'ajouter une plante en se servant du podèle Plant
		- /findId/{id} : retourne la plante correspondant à l'id indiqué
		- /findName/{name} : retourne la plante correspondant au nom indiqué
		
	- /api/guide : retourne la liste de tous les guides
		- /add-guide : permet d'ajouter un guide en se servant du podèle Guide
		- /findId/{id} : retourne le guide correspondant à l'id indiqué