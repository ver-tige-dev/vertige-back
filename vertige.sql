USE vertige;

--
-- Ajout des triggers avant suppression
--

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_photo$$
CREATE TRIGGER before_delete_photo BEFORE DELETE 
ON photo FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM plant_photo WHERE id_photo = OLD.id;
        DELETE FROM pest WHERE id_photo = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_category$$
CREATE TRIGGER before_delete_category BEFORE DELETE 
ON category FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM family WHERE id_category = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_family$$
CREATE TRIGGER before_delete_family BEFORE DELETE 
ON family FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM plant WHERE id_family = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_soil$$
CREATE TRIGGER before_delete_soil BEFORE DELETE 
ON soil FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM plant_soil WHERE id_soil = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_weather$$
CREATE TRIGGER before_delete_weather BEFORE DELETE 
ON weather FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM plant_weather WHERE id_weather = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_period$$
CREATE TRIGGER before_delete_period BEFORE DELETE 
ON period FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM trim WHERE id_period = OLD.id;
        DELETE FROM sow WHERE id_period = OLD.id;
        DELETE FROM fertilization WHERE id_period = OLD.id;
        DELETE FROM harvest WHERE id_period = OLD.id;
        DELETE FROM plantation WHERE id_period = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_pest$$
CREATE TRIGGER before_delete_pest BEFORE DELETE 
ON pest FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM plant_pest WHERE id_pest = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_plant$$
CREATE TRIGGER before_delete_plant BEFORE DELETE 
ON plant FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM avoid WHERE id_plant1 = OLD.id;
        DELETE FROM avoid WHERE id_plant2 = OLD.id;
        DELETE FROM cohabit WHERE id_plant1 = OLD.id;
        DELETE FROM cohabit WHERE id_plant2 = OLD.id;
        DELETE FROM plant_photo WHERE id_plant = OLD.id;
        DELETE FROM parcel WHERE id_plant = OLD.id;
        DELETE FROM favorites WHERE id_plant = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_user$$
CREATE TRIGGER before_delete_user BEFORE DELETE 
ON user FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM favorites WHERE id_user = OLD.id;
        DELETE FROM garden WHERE id_user = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_garden$$
CREATE TRIGGER before_delete_garden BEFORE DELETE 
ON garden FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM garden_plot WHERE id_garden = OLD.id;
    END IF;
END$$

DELIMITER ;

DELIMITER $$

DROP TRIGGER IF EXISTS before_delete_parcel$$
CREATE TRIGGER before_delete_parcel BEFORE DELETE 
ON parcel FOR EACH ROW
BEGIN
    IF(OLD.id IS NOT NULL) THEN
        DELETE FROM garden_plot WHERE id_plot = OLD.id;
    END IF;
END$$

DELIMITER ;

--
-- Ajout de données dans les tables 
--

DELETE FROM `photo`;
INSERT INTO `photo` (`id`, `name`, `url`) VALUES
(1, 'Radis', 'http://www.snv.jussieu.fr/bmedia/Marche/images/radis-1-botte.jpg');

DELETE FROM `category`;
INSERT INTO `category` (`id`, `name`, `description`) VALUES
(1, 'Fruit', NULL),
(2, 'Légume', NULL),
(3, 'Racine', NULL),
(4, 'Herbe', NULL);

DELETE FROM `family`;
INSERT INTO `family` (`id`, `name`, `description`, `id_category`) VALUES
(1, 'Tubercules', 'Test', 3);

DELETE FROM `soil`;
INSERT INTO `soil` (`id`, `name`, `description`) VALUES
(1, 'Argile', NULL),
(2, 'sable', NULL);

DELETE FROM `weather`;
INSERT INTO `weather` (`id`, `name`, `description`) VALUES
(1, 'Tempéré', NULL),
(2, 'Humide', NULL);

DELETE FROM `period`;
INSERT INTO `period` (`id`, `name`, `start_date`, `end_date`) VALUES
(1, 'Fruit', 'Mars', 'Avril');

DELETE FROM `pest`;
INSERT INTO `pest` (`id`, `name`, `description`, `treatment`, `is_disease`) VALUES
(1, 'Mildiou', NULL, NULL, true),
(2, 'Chenilles', NULL, NULL, false),
(3, 'Rongeurs', NULL, NULL, false),
(4, 'Escargots', NULL, NULL, false);

DELETE FROM `plant`;
INSERT INTO `plant` (`id`, `name`, `description`) VALUES
-- INSERT INTO `plant` (`id`, `name`, `description`, `sunshine`, `watering`, `width`, `height`, `distance`, `frost_resistance`, `hygrometry`, `isEdible`, `taste_quality`, `virtues`, `id_family`) VALUES
(1, 'Radis', NULL),
(2, 'Carottes', NULL),
(3, 'Choux', NULL),
(4, 'Petits Pois', NULL),
(5, 'Haricots', NULL),
(6, 'Panais', NULL),
(7, 'Navets', NULL),
(8, 'Concombres', NULL),
(9, 'Capucines', NULL),
(10, 'Pommes de terre', NULL),
(11, 'Aubergines', NULL),
(12, 'Maïs', NULL),
(13, 'Romarin', NULL),
(14, 'Lavande', NULL),
(15, 'Thym', NULL),
(16, 'Sarriette', NULL),
(17, 'Oignons', NULL),
(18, 'Fraises', NULL),
(19, 'Tournesols', NULL),
(20, 'Menthe', NULL),
(21, 'Agastache', NULL),
(22, 'Ail', NULL),
(23, 'Echalottes', NULL),
(24, 'Fenouil', NULL),
(25, 'Haricots grimpants', NULL),
(26, 'Salades', NULL),
(27, 'Betteraves', NULL),
(28, 'Poireaux', NULL),
(29, 'Courges', NULL),
(30, 'Tomates', NULL),
(31, 'Ciboulette', NULL),
(32, 'Cosmos', NULL),
(33, 'Persil', NULL),
(34, 'Soucis', NULL),
(35, "Oeillets d\'Inde", NULL),
(36, 'Ocas du Pérou', NULL),
(37, 'Bourraches', NULL),
(38, 'Asperges', NULL);

DELETE FROM `user`;
INSERT INTO `user` (`id`, `pseudo`, `mail`, `password`, `city`, `postal_code`) VALUES
(1, 'toto', 'mail', '937e8d5fbb48bd4949536cd65b8d35c426b80d2f830c5c308e2cdec422ae2244', 'city', 'postal_code');

DELETE FROM `garden`;
INSERT INTO `garden` (`id`, `date_create`, `id_user`) VALUES
(1, STR_TO_DATE('12/01/2021','%m/%d/%Y'), 1);

DELETE FROM `parcel`;
INSERT INTO `parcel` (`id`, `name`, `num_row`, `num_column`, `id_plant`) VALUES
(1, 'Fruit', 1, 1, 1);

--
-- Ajout de données dans les tables d'association
--

DELETE FROM `plant_photo`;
INSERT INTO `plant_photo` (`id_plant`, `id_photo`) VALUES
(1, 1);

DELETE FROM `avoid`;
INSERT INTO `avoid` (`id_plant1`, `id_plant2`) VALUES
(4, 17),
(25, 17),
(25, 22),
(25, 23),
(25, 24),
(30, 3),
(30, 10),
(30, 24);

DELETE FROM `cohabit`;
INSERT INTO `cohabit` (`id_plant1`, `id_plant2`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(5, 1),
(5, 2),
(5, 3),
(5, 6),
(5, 7),
(5, 8),
(5, 9),
(5, 10),
(5, 11),
(5, 12);

DELETE FROM `plant_soil`;
INSERT INTO `plant_soil` (`id_plant`, `id_soil`) VALUES
(1, 1),
(1, 2);

DELETE FROM `plant_weather`;
INSERT INTO `plant_weather` (`id_plant`, `id_weather`) VALUES
(1, 1),
(1, 2);

DELETE FROM `plant_pest`;
INSERT INTO `plant_pest` (`id_plant`, `id_pest`) VALUES
(1, 1),
(1, 2);

DELETE FROM `trim`;
INSERT INTO `trim` (`id_plant`, `id_period`) VALUES
(1, 1);

DELETE FROM `sow`;
INSERT INTO `sow` (`id_plant`, `id_period`) VALUES
(1, 1);

DELETE FROM `fertilization`;
INSERT INTO `fertilization` (`id_plant`, `id_period`) VALUES
(1, 1);

DELETE FROM `harvest`;
INSERT INTO `harvest` (`id_plant`, `id_period`) VALUES
(1, 1);

DELETE FROM `plantation`;
INSERT INTO `plantation` (`id_plant`, `id_period`) VALUES
(1, 1);

DELETE FROM `favorites`;
INSERT INTO `favorites` (`id_user`, `id_plant`) VALUES
(1, 1);

DELETE FROM `garden_plot`;
INSERT INTO `garden_plot` (`id_garden`, `id_plot`) VALUES
(1, 1);
