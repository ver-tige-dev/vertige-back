package fr.mds.vertige;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VertigeApplication {

	public static void main(String[] args) {
		SpringApplication.run(VertigeApplication.class, args);
	}

}
