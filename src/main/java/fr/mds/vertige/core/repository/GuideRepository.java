package fr.mds.vertige.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.mds.vertige.core.model.Guide;

@Repository
public interface GuideRepository extends JpaRepository<Guide, Long> {

}
