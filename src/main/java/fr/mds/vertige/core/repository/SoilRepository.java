package fr.mds.vertige.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.mds.vertige.core.model.Soil;

@Repository
public interface SoilRepository extends JpaRepository<Soil, Long> {

}
