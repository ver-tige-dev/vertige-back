package fr.mds.vertige.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.mds.vertige.core.model.Weather;

@Repository
public interface WeatherRepository extends JpaRepository<Weather, Long> {

}
