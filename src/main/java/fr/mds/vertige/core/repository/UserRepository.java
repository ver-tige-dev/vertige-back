package fr.mds.vertige.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.mds.vertige.core.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByPseudo(String pseudo);
	
}
