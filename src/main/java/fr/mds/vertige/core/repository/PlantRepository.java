package fr.mds.vertige.core.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.mds.vertige.core.model.Plant;

@Repository
public interface PlantRepository extends JpaRepository<Plant, Long> {

	Plant findByName(String name);
	
	@Query(value = "SELECT id_plant2 FROM avoid WHERE id_plant1 = :id UNION SELECT id_plant1 FROM avoid WHERE id_plant2 = :id", nativeQuery = true)
	Set<Long> getAvoid(Long id);
	
	@Query(value = "SELECT id_plant2 FROM cohabit WHERE id_plant1 = :id UNION SELECT id_plant1 FROM cohabit WHERE id_plant2 = :id", nativeQuery = true)
	Set<Long> getCohabit(Long id);
	
	@Query(value = "DELETE FROM cohabit WHERE id_plant1 = :id OR id_plant2 = :id", nativeQuery = true)
	void removeAvoid(Long id);

	@Query(value = "DELETE FROM cohabit WHERE id_plant1 = :id OR id_plant2 = :id", nativeQuery = true)
	void removeCohabit(Long id);

}
