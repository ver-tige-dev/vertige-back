package fr.mds.vertige.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.mds.vertige.core.model.Pest;

@Repository
public interface PestRepository extends JpaRepository<Pest, Long> {

}
