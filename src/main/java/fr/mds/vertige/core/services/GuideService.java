package fr.mds.vertige.core.services;

import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import fr.mds.vertige.core.model.Guide;
import fr.mds.vertige.core.model.GuideDTO;
import fr.mds.vertige.core.repository.GuideRepository;

@Service
public class GuideService {

    private GuideRepository repository;

    public GuideService(GuideRepository repository) {
        this.repository = repository;
    }


    public Guide registerNewGuide(@Validated GuideDTO guideDto) {
        return repository.save(createGuide(guideDto));
    }
    
    private Guide createGuide(GuideDTO guideDto) {

    	Guide newGuide = new Guide();

    	// les 2 lignes suivantes sont a revérifier!
//    	newGuide.getPlant().setAvoid(guideDto.getPlant().getAvoid());
//    	newGuide.getPlant().setCohabit(guideDto.getPlant().getCohabit());
    	
//    	newGuide.setDistance(guideDto.getDistance());
//    	newGuide.setFamily(guideDto.getFamily());
//    	newGuide.setFertilization(guideDto.getFertilization());
//    	newGuide.setFrost_resistance(guideDto.getFrost_resistance());
//    	newGuide.setHarvest(guideDto.getHarvest());
//    	newGuide.setHeight(guideDto.getHeight());
//    	newGuide.setHygrometry(guideDto.getHygrometry());
//    	newGuide.setIsEdible(guideDto.getIsEdible());
//    	newGuide.setPests(guideDto.getPests());
//    	newGuide.setPlant(guideDto.getPlant());
//    	newGuide.setPlantation(guideDto.getPlantation());
//    	newGuide.setSoils(guideDto.getSoils());
//    	newGuide.setSow(guideDto.getSow());
//    	newGuide.setSunshine(guideDto.getSunshine());
//    	newGuide.setTaste_quality(guideDto.getTaste_quality());
//    	newGuide.setTrim(guideDto.getTrim());
//    	newGuide.setVirtues(guideDto.getVirtues());
//    	newGuide.setWatering(guideDto.getWatering());
//    	newGuide.setWeathers(guideDto.getWeathers());
//    	newGuide.setWidth(guideDto.getWidth());

//        return newGuide;
    	return null;
    }
}
