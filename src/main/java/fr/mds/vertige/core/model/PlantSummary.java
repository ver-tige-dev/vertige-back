package fr.mds.vertige.core.model;

import java.util.HashSet;
import java.util.Set;

public class PlantSummary {
	
	private Long id;
	private String name;
	private String description;
    private Set<Photo> photos = new HashSet<>();
    private Set<Long> avoid = new HashSet<>();
    private Set<Long> cohabit = new HashSet<>();
	
	private Double sunshine;
	private Double watering;
	private Double width;
	private Double height;
	private Double distance;
	private Double frost_resistance;
	private Double hygrometry;
	private Boolean isEdible;
	private int taste_quality;
	private String virtues;
    
	private Family family;
    private Set<Soil> soils = new HashSet<>();
    private Set<Weather> weathers = new HashSet<>();
    private Set<Pest> pests = new HashSet<>();
    private Set<Period> trim = new HashSet<>();
    private Set<Period> sow = new HashSet<>();
    private Set<Period> fertilization = new HashSet<>();
    private Set<Period> harvest = new HashSet<>();
    private Set<Period> plantation = new HashSet<>();
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Set<Photo> getPhotos() {
		return photos;
	}
	public void setPhotos(Set<Photo> photos) {
		this.photos = photos;
	}
	public Set<Long> getAvoid() {
		return avoid;
	}
	public void setAvoid(Set<Long> avoid) {
		this.avoid = avoid;
	}
	public Set<Long> getCohabit() {
		return cohabit;
	}
	public void setCohabit(Set<Long> cohabit) {
		this.cohabit = cohabit;
	}
	
	public Double getSunshine() {
		return sunshine;
	}
	public void setSunshine(Double sunshine) {
		this.sunshine = sunshine;
	}
	public Double getWatering() {
		return watering;
	}
	public void setWatering(Double watering) {
		this.watering = watering;
	}
	public Double getWidth() {
		return width;
	}
	public void setWidth(Double width) {
		this.width = width;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public Double getFrost_resistance() {
		return frost_resistance;
	}
	public void setFrost_resistance(Double frost_resistance) {
		this.frost_resistance = frost_resistance;
	}
	public Double getHygrometry() {
		return hygrometry;
	}
	public void setHygrometry(Double hygrometry) {
		this.hygrometry = hygrometry;
	}
	public Boolean getIsEdible() {
		return isEdible;
	}
	public void setIsEdible(Boolean isEdible) {
		this.isEdible = isEdible;
	}
	public int getTaste_quality() {
		return taste_quality;
	}
	public void setTaste_quality(int taste_quality) {
		this.taste_quality = taste_quality;
	}
	public String getVirtues() {
		return virtues;
	}
	public void setVirtues(String virtues) {
		this.virtues = virtues;
	}
	
	public Family getFamily() {
		return family;
	}
	public void setFamily(Family family) {
		this.family = family;
	}
	public Set<Soil> getSoils() {
		return soils;
	}
	public void setSoils(Set<Soil> soils) {
		this.soils = soils;
	}
	public Set<Weather> getWeathers() {
		return weathers;
	}
	public void setWeathers(Set<Weather> weathers) {
		this.weathers = weathers;
	}
	public Set<Pest> getPests() {
		return pests;
	}
	public void setPests(Set<Pest> pests) {
		this.pests = pests;
	}
	public Set<Period> getTrim() {
		return trim;
	}
	public void setTrim(Set<Period> trim) {
		this.trim = trim;
	}
	public Set<Period> getSow() {
		return sow;
	}
	public void setSow(Set<Period> sow) {
		this.sow = sow;
	}
	public Set<Period> getFertilization() {
		return fertilization;
	}
	public void setFertilization(Set<Period> fertilization) {
		this.fertilization = fertilization;
	}
	public Set<Period> getHarvest() {
		return harvest;
	}
	public void setHarvest(Set<Period> harvest) {
		this.harvest = harvest;
	}
	public Set<Period> getPlantation() {
		return plantation;
	}
	public void setPlantation(Set<Period> plantation) {
		this.plantation = plantation;
	}
    
}
