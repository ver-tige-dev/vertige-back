package fr.mds.vertige.core.model;

import java.util.HashSet;
import java.util.Set;

public class UserSummary {
	
	private Long id;
	private String pseudo;
	private String mail;
	private String password;
	private String city;
	private String postal_code;
    private Set<Long> favorites = new HashSet<>();
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPostal_code() {
		return postal_code;
	}
	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}
	public Set<Long> getFavorites() {
		return favorites;
	}
	public void setFavorites(Set<Long> favorites) {
		this.favorites = favorites;
	}
    
}
