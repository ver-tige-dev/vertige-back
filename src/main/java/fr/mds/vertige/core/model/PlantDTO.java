package fr.mds.vertige.core.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

public class PlantDTO {
	
	private String name;
	private String description;
    private Set<Long> photos = new HashSet<>();
    
    private Double sunshine;
	private Double watering;
	private Double width;
	private Double height;
	private Double distance;
	private Double frost_resistance;
	private Double hygrometry;
	private Boolean isEdible;
	private int taste_quality;
	private String virtues;
    
    private Set<Long> avoid = new HashSet<>();
    private Set<Long> cohabit = new HashSet<>();
    
	private Long family;
    private Set<Long> soils = new HashSet<>();
    private Set<Long> weathers = new HashSet<>();
    private Set<Long> pests = new HashSet<>();
    private Set<Long> trim = new HashSet<>();
    private Set<Long> sow = new HashSet<>();
    private Set<Long> fertilization = new HashSet<>();
    private Set<Long> harvest = new HashSet<>();
    private Set<Long> plantation = new HashSet<>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Set<Long> getPhotos() {
		return photos;
	}
	public void setPhotos(Set<Long> photos) {
		this.photos = photos;
	}
	
	public Double getSunshine() {
		return sunshine;
	}
	public void setSunshine(Double sunshine) {
		this.sunshine = sunshine;
	}
	public Double getWatering() {
		return watering;
	}
	public void setWatering(Double watering) {
		this.watering = watering;
	}
	public Double getWidth() {
		return width;
	}
	public void setWidth(Double width) {
		this.width = width;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public Double getFrost_resistance() {
		return frost_resistance;
	}
	public void setFrost_resistance(Double frost_resistance) {
		this.frost_resistance = frost_resistance;
	}
	public Double getHygrometry() {
		return hygrometry;
	}
	public void setHygrometry(Double hygrometry) {
		this.hygrometry = hygrometry;
	}
	public Boolean getIsEdible() {
		return isEdible;
	}
	public void setIsEdible(Boolean isEdible) {
		this.isEdible = isEdible;
	}
	public int getTaste_quality() {
		return taste_quality;
	}
	public void setTaste_quality(int taste_quality) {
		this.taste_quality = taste_quality;
	}
	public String getVirtues() {
		return virtues;
	}
	public void setVirtues(String virtues) {
		this.virtues = virtues;
	}
	public Long getFamily() {
		return family;
	}
	public void setFamily(Long family) {
		this.family = family;
	}
	public Set<Long> getSoils() {
		return soils;
	}
	public void setSoils(Set<Long> soils) {
		this.soils = soils;
	}
	public Set<Long> getWeathers() {
		return weathers;
	}
	public void setWeathers(Set<Long> weathers) {
		this.weathers = weathers;
	}
	public Set<Long> getPests() {
		return pests;
	}
	public void setPests(Set<Long> pests) {
		this.pests = pests;
	}
	public Set<Long> getTrim() {
		return trim;
	}
	public void setTrim(Set<Long> trim) {
		this.trim = trim;
	}
	public Set<Long> getSow() {
		return sow;
	}
	public void setSow(Set<Long> sow) {
		this.sow = sow;
	}
	public Set<Long> getFertilization() {
		return fertilization;
	}
	public void setFertilization(Set<Long> fertilization) {
		this.fertilization = fertilization;
	}
	public Set<Long> getHarvest() {
		return harvest;
	}
	public void setHarvest(Set<Long> harvest) {
		this.harvest = harvest;
	}
	public Set<Long> getPlantation() {
		return plantation;
	}
	public void setPlantation(Set<Long> plantation) {
		this.plantation = plantation;
	}
	public Set<Long> getAvoid() {
		return avoid;
	}
	public void setAvoid(Set<Long> avoid) {
		this.avoid = avoid;
	}
	public Set<Long> getCohabit() {
		return cohabit;
	}
	public void setCohabit(Set<Long> cohabit) {
		this.cohabit = cohabit;
	}
	
}
