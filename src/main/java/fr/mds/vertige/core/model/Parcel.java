package fr.mds.vertige.core.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "parcel")
public class Parcel {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	private int num_row;
	private int num_column;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_plant", referencedColumnName = "id")
	private Plant plant;

	public Parcel(int num_row, int num_column) {
		this.num_row = num_row;
		this.num_column = num_column;
	}

	public Parcel(String name, int num_row, int num_column) {
		this.name = name;
		this.num_row = num_row;
		this.num_column = num_column;
	}

	public Parcel(String name, int num_row, int num_column, Plant plant) {
		this.name = name;
		this.num_row = num_row;
		this.num_column = num_column;
		this.plant = plant;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNum_row() {
		return num_row;
	}
	public void setNum_row(int nb_row) {
		this.num_row = nb_row;
	}
	public int getNum_column() {
		return num_column;
	}
	public void setNum_column(int nb_column) {
		this.num_column = nb_column;
	}
	public Plant getPlant() {
		return plant;
	}
	public void setPlant(Plant plant) {
		this.plant = plant;
	}

}
