package fr.mds.vertige.core.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "guide")
public class Guide {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;


//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "id_plant", referencedColumnName = "id")
//	private Plant plant;

    
    
	public Guide() {}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
//	public Plant getPlant() {
//		return plant;
//	}
//	public void setPlant(Plant plant) {
//		this.plant = plant;
//	}
	
}