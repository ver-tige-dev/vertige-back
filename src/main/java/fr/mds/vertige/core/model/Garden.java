package fr.mds.vertige.core.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "garden")
public class Garden {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Date date_create;
	
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_user", referencedColumnName = "id")
	private User user;
	
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "garden_parcel",
                joinColumns = @JoinColumn(name = "id_garden"),
                inverseJoinColumns = @JoinColumn(name = "id_parcel"))
    private Set<Parcel> parcels = new HashSet<>();
    
    private int row;
    private int col;

	public Garden(Date date_create, User user, Set<Parcel> parcels) {
		this.date_create = date_create;
		this.user = user;
		this.parcels = parcels;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDate_create() {
		return date_create;
	}
	public void setDate_create(Date date_create) {
		this.date_create = date_create;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<Parcel> getPlots() {
		return parcels;
	}
	public void setPlots(Set<Parcel> plots) {
		this.parcels = plots;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}
	
	
    
}
