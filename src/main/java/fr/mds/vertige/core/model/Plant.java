package fr.mds.vertige.core.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "plant")
public class Plant {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String name;
	private String description;
	
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "plant_photo",
                joinColumns = @JoinColumn(name = "id_plant"),
                inverseJoinColumns = @JoinColumn(name = "id_photo"))
    private Set<Photo> photos = new HashSet<>();
	
	private Double sunshine;
	private Double watering;
	private Double width;
	private Double height;
	private Double distance;
	private Double frost_resistance;
	private Double hygrometry;
	private Boolean isEdible;
	@Column(columnDefinition = "int default 0")
	private int taste_quality;
	private String virtues;
    
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "avoid",
                joinColumns = @JoinColumn(name = "id_plant1"),
                inverseJoinColumns = @JoinColumn(name = "id_plant2"))
    private Set<Plant> avoid = new HashSet<>();
    
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "cohabit",
                joinColumns = @JoinColumn(name = "id_plant1"),
                inverseJoinColumns = @JoinColumn(name = "id_plant2"))
    private Set<Plant> cohabit = new HashSet<>();
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_family", referencedColumnName = "id")
	private Family family;
	
	@ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "plant_soil",
                joinColumns = @JoinColumn(name = "id_plant"),
                inverseJoinColumns = @JoinColumn(name = "id_soil"))
    private Set<Soil> soils = new HashSet<>();
	
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "plant_weather",
                joinColumns = @JoinColumn(name = "id_plant"),
                inverseJoinColumns = @JoinColumn(name = "id_weather"))
    private Set<Weather> weathers = new HashSet<>();
	
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "plant_pest",
                joinColumns = @JoinColumn(name = "id_plant"),
                inverseJoinColumns = @JoinColumn(name = "id_pest"))
    private Set<Pest> pests = new HashSet<>();
    
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "trim",
                joinColumns = @JoinColumn(name = "id_plant"),
                inverseJoinColumns = @JoinColumn(name = "id_period"))
    private Set<Period> trim = new HashSet<>();
    
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "sow",
                joinColumns = @JoinColumn(name = "id_plant"),
                inverseJoinColumns = @JoinColumn(name = "id_period"))
    private Set<Period> sow = new HashSet<>();
    
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "fertilization",
                joinColumns = @JoinColumn(name = "id_plant"),
                inverseJoinColumns = @JoinColumn(name = "id_period"))
    private Set<Period> fertilization = new HashSet<>();
    
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "harvest",
                joinColumns = @JoinColumn(name = "id_plant"),
                inverseJoinColumns = @JoinColumn(name = "id_period"))
    private Set<Period> harvest = new HashSet<>();
    
    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(	name = "plantation",
                joinColumns = @JoinColumn(name = "id_plant"),
                inverseJoinColumns = @JoinColumn(name = "id_period"))
    private Set<Period> plantation = new HashSet<>();
	
	public Plant() {}

	public Plant(String name, String description) {
		this.name = name;
		this.description = description;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Set<Photo> getPhotos() {
		return photos;
	}
	public void setPhotos(Set<Photo> photos) {
		this.photos = photos;
	}
	public Double getSunshine() {
		return sunshine;
	}
	public void setSunshine(Double sunshine) {
		this.sunshine = sunshine;
	}
	public Double getWatering() {
		return watering;
	}
	public void setWatering(Double watering) {
		this.watering = watering;
	}
	public Double getWidth() {
		return width;
	}
	public void setWidth(Double width) {
		this.width = width;
	}
	public Double getHeight() {
		return height;
	}
	public void setHeight(Double height) {
		this.height = height;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	public Double getFrost_resistance() {
		return frost_resistance;
	}
	public void setFrost_resistance(Double frost_resistance) {
		this.frost_resistance = frost_resistance;
	}
	public Double getHygrometry() {
		return hygrometry;
	}
	public void setHygrometry(Double hygrometry) {
		this.hygrometry = hygrometry;
	}
	public Boolean getIsEdible() {
		return isEdible;
	}
	public void setIsEdible(Boolean isEdible) {
		this.isEdible = isEdible;
	}
	public int getTaste_quality() {
		return taste_quality;
	}
	public void setTaste_quality(int taste_quality) {
		this.taste_quality = taste_quality;
	}
	public String getVirtues() {
		return virtues;
	}
	public void setVirtues(String virtues) {
		this.virtues = virtues;
	}
	public Set<Plant> getAvoid() {
		return avoid;
	}
	public void setAvoid(Set<Plant> avoid) {
		this.avoid = avoid;
	}
	public Set<Plant> getCohabit() {
		return cohabit;
	}
	public void setCohabit(Set<Plant> cohabit) {
		this.cohabit = cohabit;
	}
	public Family getFamily() {
		return family;
	}
	public void setFamily(Family family) {
		this.family = family;
	}
	public Set<Soil> getSoils() {
		return soils;
	}
	public void setSoils(Set<Soil> soils) {
		this.soils = soils;
	}
	public Set<Weather> getWeathers() {
		return weathers;
	}
	public void setWeathers(Set<Weather> weathers) {
		this.weathers = weathers;
	}
	public Set<Pest> getPests() {
		return pests;
	}
	public void setPests(Set<Pest> pests) {
		this.pests = pests;
	}
	public Set<Period> getTrim() {
		return trim;
	}
	public void setTrim(Set<Period> trim) {
		this.trim = trim;
	}
	public Set<Period> getSow() {
		return sow;
	}
	public void setSow(Set<Period> sow) {
		this.sow = sow;
	}
	public Set<Period> getFertilization() {
		return fertilization;
	}
	public void setFertilization(Set<Period> fertilization) {
		this.fertilization = fertilization;
	}
	public Set<Period> getHarvest() {
		return harvest;
	}
	public void setHarvest(Set<Period> harvest) {
		this.harvest = harvest;
	}
	public Set<Period> getPlantation() {
		return plantation;
	}
	public void setPlantation(Set<Period> plantation) {
		this.plantation = plantation;
	}	
	
}
