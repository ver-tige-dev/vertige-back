package fr.mds.vertige.core.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Weather;
import fr.mds.vertige.core.repository.WeatherRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/weather")
public class WeatherController {
	private WeatherRepository repository;

    public WeatherController(WeatherRepository repository) {
        this.repository = repository;
    }

    @PostMapping("/add-weather")
    public Weather addWeather(@Validated @RequestBody Weather weather) {
        return repository.save(weather);
    }

    @GetMapping
    public ResponseEntity<List<Weather>> getAll() {
        List<Weather> weathers = repository.findAll();
        return ResponseEntity.ok().body(weathers);
    }

    @GetMapping("/findId/{id}")
    public Optional<Weather> findById(@PathVariable(value = "id") long id) {
        Optional<Weather> weather = repository.findById(id);
        return weather;
    }
    
    @PutMapping("/edit/{id}")
    public void updateWeather(@PathVariable(value = "id") Long id, @Valid @RequestBody Weather weather) {
    	Weather base = repository.findById(id).get();
    	base.setName(weather.getName());
    	base.setDescription(weather.getDescription());
    	
    	repository.save(base);
    }

    @DeleteMapping("/remove/{id}")
    public void removeById(@PathVariable(value = "id") Long id) {
    	// créer trigger por supprimer les assos
        repository.deleteById(id);
    }

}
