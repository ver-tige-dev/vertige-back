package fr.mds.vertige.core.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Period;
import fr.mds.vertige.core.repository.PeriodRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/period")
public class PeriodController {
	private PeriodRepository repository;

    public PeriodController(PeriodRepository repository) {
        this.repository = repository;
    }

    @PostMapping("/add-period")
    public Period addPeriod(@Validated @RequestBody String name) {
        return repository.save(new Period(name));
    }

    @GetMapping
    public ResponseEntity<List<Period>> getAll() {
        List<Period> periods = repository.findAll();
        return ResponseEntity.ok().body(periods);
    }

    @GetMapping("/findId/{id}")
    public Optional<Period> findById(@PathVariable(value = "id") long id) {
        Optional<Period> period = repository.findById(id);
        return period;
    }
    
    @PutMapping("/edit/{id}")
    public void updatePeriod(@PathVariable(value = "id") Long id, @Valid @RequestBody Period period) {
    	Period base = repository.findById(id).get();
    	base.setName(period.getName());
    	base.setStart_date(period.getStart_date());
    	base.setEnd_date(period.getEnd_date());
    	
    	repository.save(base);
    }

    @DeleteMapping("/remove/{id}")
    public void removeById(@PathVariable(value = "id") Long id) {
        repository.deleteById(id);
    }

}
