package fr.mds.vertige.core.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Parcel;
import fr.mds.vertige.core.repository.ParcelRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/plot")
public class ParcelController {
	private ParcelRepository repository;

    public ParcelController(ParcelRepository repository) {
        this.repository = repository;
    }

    @PostMapping("/add-plot")
    public Parcel addPlot(@Validated @RequestBody Parcel plot) {
    	return repository.save(plot);
    }

    @GetMapping
    public ResponseEntity<List<Parcel>> getAll() {
        List<Parcel> plots = repository.findAll();
        return ResponseEntity.ok().body(plots);
    }

    @GetMapping("/findId/{id}")
    public Optional<Parcel> findById(@PathVariable(value = "id") long id) {
        Optional<Parcel> plot = repository.findById(id);
        return plot;
    }

}
