package fr.mds.vertige.core.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Entity;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Guide;
import fr.mds.vertige.core.model.GuideDTO;
import fr.mds.vertige.core.model.GuideSummary;
import fr.mds.vertige.core.repository.GuideRepository;
import fr.mds.vertige.core.services.GuideService;

@RestController
@CrossOrigin
@RequestMapping("/api/guide")
public class GuideController {
	
	private GuideRepository repository;
	private GuideService service;

    public GuideController(GuideRepository repository, GuideService service) {
        this.repository = repository;
        this.service = service;
    }

    @PostMapping("/add-guide")
    public Guide addGuide(@Validated @RequestBody GuideDTO guide) {
    	return service.registerNewGuide(guide);
    }

    @GetMapping
    public ResponseEntity<List<GuideSummary>> getAll() {
    	System.out.println("toto");
    	List<Guide> datas = repository.findAll();
    	System.out.println(datas);
        List<GuideSummary> guides = new ArrayList<GuideSummary>();
        
        Iterator<Guide> iterator = datas.iterator();
	    while ( iterator.hasNext() ) {
	    	guides.add(getSummary(iterator.next()));
	    }
        return ResponseEntity.ok().body(guides);
    }

    @GetMapping("/findId/{id}")
    public Optional<Guide> findById(@PathVariable(value = "id") long id) {
        Optional<Guide> guide = repository.findById(id);
        return guide;
    }
    
    public GuideSummary getSummary(Guide guide) {
//    	GuideSummary summ = new GuideSummary();
//    	summ.setSunshine(guide.getSunshine());
//    	summ.setWatering(guide.getWatering());
//    	summ.setWidth(guide.getWidth());
//    	summ.setHeight(guide.getHeight());
//    	summ.setDistance(guide.getDistance());
//    	summ.setFrost_resistance(guide.getFrost_resistance());
//    	summ.setHygrometry(guide.getHygrometry());
//    	summ.setIsEdible(guide.getIsEdible());
//    	summ.setTaste_quality(guide.getTaste_quality());
//    	summ.setVirtues(guide.getVirtues());
//    	summ.setPlant(guide.getPlant());
//    	summ.setFamily(guide.getFamily());
//
//    	summ.setSoils(getIds(guide.getSoils(), "soil"));   	
//    	summ.setWeathers(getIds(guide.getWeathers(), "weather"));
//    	summ.setPests(getIds(guide.getPests(), "pest"));
//    	summ.setTrim(getIds(guide.getTrim(), "period"));
//    	summ.setSow(getIds(guide.getSow(), "period"));
//    	summ.setFertilization(getIds(guide.getFertilization(), "period"));
//    	summ.setHarvest(getIds(guide.getHarvest(), "period"));
//    	summ.setPlantation(getIds(guide.getPlantation(), "period"));
    	
//    	summ.setAvoid(plantController.getAvoid(guide.getPlant().getId()));
//    	summ.setCohabit(plantController.getCohabit(guide.getPlant().getId()));
    	
//    	return summ;
    	return null;
    }

}
