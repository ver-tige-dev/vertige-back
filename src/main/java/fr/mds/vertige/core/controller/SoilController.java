package fr.mds.vertige.core.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Soil;
import fr.mds.vertige.core.repository.SoilRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/soil")
public class SoilController {
	private SoilRepository repository;

    public SoilController(SoilRepository repository) {
        this.repository = repository;
    }

    @PostMapping
    public Soil addSoil(@Validated @RequestBody Soil soil) {
        return repository.save(soil);
    }

    @GetMapping
    public ResponseEntity<List<Soil>> getAll() {
        List<Soil> soils = repository.findAll();
        return ResponseEntity.ok().body(soils);
    }

    @GetMapping("/findId/{id}")
    public Optional<Soil> findById(@PathVariable(value = "id") long id) {
        Optional<Soil> soil = repository.findById(id);
        return soil;
    }
    
    @PutMapping("/edit/{id}")
    public void updateSoil(@PathVariable(value = "id") Long id, @Valid @RequestBody Soil soil) {
    	Soil base = repository.findById(id).get();
    	base.setName(soil.getName());
    	base.setDescription(soil.getDescription());
    	
    	repository.save(base);
    }

    @DeleteMapping("/remove/{id}")
    public void removeById(@PathVariable(value = "id") Long id) {
        repository.deleteById(id);
    }

}
