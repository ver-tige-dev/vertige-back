package fr.mds.vertige.core.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Plant;
import fr.mds.vertige.core.model.User;
import fr.mds.vertige.core.model.UserSummary;
import fr.mds.vertige.core.repository.PlantRepository;
import fr.mds.vertige.core.repository.UserRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/user")
public class UserController {
	
	private String encode = "SHA-256";
	
	private UserRepository repository;
	private PlantRepository plantRepository;
	
    public UserController(UserRepository repository, PlantRepository plantRepository) {
        this.repository = repository;
        this.plantRepository = plantRepository;
    }

    @PostMapping("/register")
    public User Register(@Validated @RequestBody User user) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(encode);
	    md.update(user.getPassword().toLowerCase().getBytes());
	    byte[] digest = md.digest();
	    String myHash = DatatypeConverter.printHexBinary(digest).toLowerCase();
	    
    	user.setPassword(myHash);
    	return repository.save(user);
    }
    
    @PostMapping("/login")
	public ResponseEntity<User> login(@Valid @RequestBody String[] data) throws NoSuchAlgorithmException {
    	User user = repository.findByPseudo(data[0]);
		if (user != null) {
			
			MessageDigest md = MessageDigest.getInstance(encode);
		    md.update(data[1].toLowerCase().getBytes());
		    byte[] digest = md.digest();
		    String myHash = DatatypeConverter.printHexBinary(digest).toLowerCase();

			if (myHash.equals(user.getPassword())) {
				System.out.println("ok");
				return ResponseEntity.ok(user);
			} else {
				System.out.println("not ok");
				return ResponseEntity.notFound().build(); // return error 404, undefined
			}
		} else {
			System.out.println("not ok");
			return ResponseEntity.notFound().build(); // return error 404, undefined
		}
	}

    @GetMapping
    public ResponseEntity<List<UserSummary>> getAll() {
        List<User> users = repository.findAll();
        List<UserSummary> usersSum = new ArrayList<UserSummary>();

        Iterator<User> iterator = users.iterator();
	    while ( iterator.hasNext() ) {
	    	usersSum.add(getSummary(iterator.next()));
	    }
        return ResponseEntity.ok().body(usersSum);
    }
    
    public UserSummary getSummary(User user) {
    	UserSummary summ = new UserSummary();
    	
    	summ.setId(user.getId());
    	summ.setPseudo(user.getPseudo());
    	summ.setMail(user.getMail());
    	summ.setPassword(user.getPassword());
    	summ.setCity(user.getCity());
    	summ.setPostal_code(user.getPostal_code());

		Iterator<Plant> iterator = user.getFavorites().iterator();
    	Set<Long> list = new HashSet<Long>();
	    while ( iterator.hasNext() ) {
	    	list.add(((Plant) iterator.next()).getId());
	    }
    	summ.setFavorites(list);
    	return summ;
    }
    
    @GetMapping("/findId/{id}")
    public ResponseEntity<UserSummary> findById(@PathVariable(value = "id") long id) {
        try {
			User user = repository.findById(id).get();
			return ResponseEntity.ok().body(getSummary(user));
		} catch (Exception e) {
			return ResponseEntity.notFound().build();
		}
    }

    @GetMapping("/findPseudo/{pseudo}")
    public User findByPseudo(@PathVariable(value = "pseudo") String pseudo) {
        User user = repository.findByPseudo(pseudo);
        return user;
    }
    
    @PutMapping("/edit/{id}")
    public void updateUser(@PathVariable(value = "id") Long id, @Valid @RequestBody User user) {
    	User baseUser = repository.findById(id).get();
    	baseUser.setCity(user.getCity());
    	baseUser.setFavorites(user.getFavorites());
    	baseUser.setMail(user.getMail());
    	// baseUser.setPassword(user.getPassword());
    	baseUser.setPostal_code(user.getPostal_code());
    	// baseUser.setPseudo(user.getPseudo());
    	
    	repository.save(baseUser);
    }

    @DeleteMapping("/remove/{id}")
    public void removeById(@PathVariable(value = "id") Long id) {
        repository.deleteById(id);
    }
    
    @PutMapping("/favs/{idUser}/{idPlant}")
    public void updateFavs(@PathVariable(value = "idUser") Long idUser, @PathVariable(value = "idPlant") Long idPlant) {
    	User user = repository.findById(idUser).get();
    	Plant plant = plantRepository.findById(idPlant).get();
    	if (user.getFavorites().contains(plant)) {
    		user.getFavorites().remove(plant);
    	} else {
    		user.getFavorites().add(plant);
    	}
    	repository.save(user);
    }
}
