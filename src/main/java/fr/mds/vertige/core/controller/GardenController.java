package fr.mds.vertige.core.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Garden;
import fr.mds.vertige.core.model.Parcel;
import fr.mds.vertige.core.repository.GardenRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/garden")
public class GardenController {
	
	private GardenRepository repository;
	private ParcelController parcelController;

    public GardenController(GardenRepository repository, ParcelController parcelController) {
        this.repository = repository;
        this.parcelController = parcelController;
    }

    @PostMapping("/add-garden")
    public Garden addGarden(@Validated @RequestBody Garden garden) {
    	
    	try {
			int rows = garden.getRow();
			int cols = garden.getCol();
			
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					Parcel plot = new Parcel(i, j);
					garden.getPlots().add(parcelController.addPlot(plot));
				}			
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return repository.save(garden);
    }

    @PutMapping("/update-garden")
    public Garden updateGarden(@Validated @RequestBody Garden garden) {
    	
    	long id = garden.getId();
    	
    	Garden base = repository.getOne(id);
    	
    	if (base.getRow() != garden.getRow() || base.getCol() != garden.getCol()) {
    		// clear tte les cases et tout recréer ???
        	
        	try {
    			int rows = garden.getRow();
    			int cols = garden.getCol();
    			
    			for (int i = 0; i < rows; i++) {
    				for (int j = 0; j < cols; j++) {
    					Parcel plot = new Parcel(i, j);
    					// ajouter les informations de la parcelle
    					garden.getPlots().add(parcelController.addPlot(plot));
    				}			
    			}
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
        	
        	return repository.save(garden);
    	} else {
    		// boucler sur chaque case et vérifier si modif
    		return null;
    	}
    }

    @GetMapping
    public ResponseEntity<List<Garden>> getAll() {
        List<Garden> gardens = repository.findAll();
        return ResponseEntity.ok().body(gardens);
    }

    @GetMapping("/findId/{id}")
    public Optional<Garden> findById(@PathVariable(value = "id") long id) {
        Optional<Garden> garden = repository.findById(id);
        return garden;
    }

}
