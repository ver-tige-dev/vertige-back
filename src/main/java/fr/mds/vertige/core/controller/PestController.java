package fr.mds.vertige.core.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Pest;
import fr.mds.vertige.core.repository.PestRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/pest")
public class PestController {
	private PestRepository repository;

    public PestController(PestRepository repository) {
        this.repository = repository;
    }

    @PostMapping("/add-pest")
    public Pest addPest(@Validated @RequestBody String name) {
        return repository.save(new Pest(name));
    }

    @GetMapping
    public ResponseEntity<List<Pest>> getAll() {
        List<Pest> pests = repository.findAll();
        return ResponseEntity.ok().body(pests);
    }

    @GetMapping("/findId/{id}")
    public Optional<Pest> findById(@PathVariable(value = "id") long id) {
        Optional<Pest> pest = repository.findById(id);
        return pest;
    }
    
    @PutMapping("/edit/{id}")
    public void updatePest(@PathVariable(value = "id") Long id, @Valid @RequestBody Pest pest) {
    	Pest base = repository.findById(id).get();
    	base.setName(pest.getName());
    	base.setDescription(pest.getDescription());
    	
    	repository.save(base);
    }

    @DeleteMapping("/remove/{id}")
    public void removeById(@PathVariable(value = "id") Long id) {
        repository.deleteById(id);
    }

}
