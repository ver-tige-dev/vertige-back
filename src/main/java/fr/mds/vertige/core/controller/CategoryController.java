package fr.mds.vertige.core.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Category;
import fr.mds.vertige.core.model.User;
import fr.mds.vertige.core.repository.CategoryRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/category")
public class CategoryController {
	private CategoryRepository repository;

    public CategoryController(CategoryRepository repository) {
        this.repository = repository;
    }

    @PostMapping
    public Category addCategory(@Validated @RequestBody Category category) {
        return repository.save(category);
    }

    @GetMapping
    public ResponseEntity<List<Category>> getAll() {
        List<Category> categories = repository.findAll();
        return ResponseEntity.ok().body(categories);
    }

    @GetMapping("/findId/{id}")
    public Optional<Category> findById(@PathVariable(value = "id") long id) {
        Optional<Category> category = repository.findById(id);
        return category;
    }
    
    @PutMapping("/edit/{id}")
    public void updateCategory(@PathVariable(value = "id") Long id, @Valid @RequestBody Category category) {
    	Category base = repository.findById(id).get();
    	base.setName(category.getName());
    	base.setDescription(category.getDescription());
    	
    	repository.save(base);
    }

    @DeleteMapping("/remove/{id}")
    public void removeById(@PathVariable(value = "id") Long id) {
        repository.deleteById(id);
    }

}
