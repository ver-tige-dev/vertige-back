package fr.mds.vertige.core.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Period;
import fr.mds.vertige.core.model.Pest;
import fr.mds.vertige.core.model.Photo;
import fr.mds.vertige.core.model.Plant;
import fr.mds.vertige.core.model.PlantDTO;
import fr.mds.vertige.core.model.PlantSummary;
import fr.mds.vertige.core.model.Soil;
import fr.mds.vertige.core.model.Weather;
import fr.mds.vertige.core.repository.PeriodRepository;
import fr.mds.vertige.core.repository.PestRepository;
import fr.mds.vertige.core.repository.PhotoRepository;
import fr.mds.vertige.core.repository.PlantRepository;
import fr.mds.vertige.core.repository.SoilRepository;
import fr.mds.vertige.core.repository.WeatherRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/plant")
public class PlantController {
	
	private PlantRepository repository;
	private PhotoRepository photoRep;
	private SoilRepository soilRep;
	private WeatherRepository weatherRep;
	private PestRepository pestRep;
	private PeriodRepository periodRep;

    public PlantController(PlantRepository repository, PhotoRepository photoRep, 
    		SoilRepository soilRep, WeatherRepository weatherRep, 
    		PestRepository pestRep, PeriodRepository periodRep) {
        this.repository = repository;
        this.photoRep = photoRep;
    	this.soilRep = soilRep;
    	this.weatherRep = weatherRep;
    	this.pestRep = pestRep;
    	this.periodRep = periodRep;
    }

    @PostMapping
    public PlantSummary addPlant(@Validated @RequestBody PlantDTO dto) {
    	Plant plant = new Plant(dto.getName(), dto.getDescription());
        repository.save(plant);
        return getSummary(plant);
    }

    @GetMapping
    public ResponseEntity<List<PlantSummary>> getAll() {
    	List<Plant> datas = repository.findAll();
        List<PlantSummary> plantsSumm = new ArrayList<PlantSummary>();
        
        Iterator<Plant> iterator = datas.iterator();
	    while ( iterator.hasNext() ) {
	    	plantsSumm.add(getSummary(iterator.next()));
	    }
        return ResponseEntity.ok().body(plantsSumm);
    }

    @GetMapping("/findId/{id}")
    public PlantSummary findById(@PathVariable(value = "id") long id) {
        Plant plant = repository.findById(id).get();
        PlantSummary summ = getSummary(plant);
        return summ;
    }

    @GetMapping("/find-name/{name}")
    public PlantSummary findByName(@PathVariable(value = "name") String name) {
    	Plant plant = repository.findByName(name);
    	PlantSummary summ = getSummary(plant);
        return summ;
    }
    
    @DeleteMapping("/remove/{id}")
    public void removeById(@PathVariable(value = "id") Long id) {
    	// Trigger créé pour supprimer dans les tables avoid, cohabit, plant_photo, plot_plant, favorites
        repository.deleteById(id);
    }
    
    @PutMapping("/update/{id}")
    public void updatePlant(@PathVariable(value = "id") Long id, @Validated @RequestBody PlantDTO dto) {
    	Plant plant = repository.findById(id).get();
    	plant.setName(dto.getName());
    	plant.setDescription(dto.getDescription());
    	plant.setPhotos(getSetFromLong(dto.getPhotos(), "photo"));
    	
    	plant.setSunshine(dto.getSunshine());
    	plant.setWatering(dto.getWatering());
    	plant.setWidth(dto.getWidth());
    	plant.setHeight(dto.getHeight());
    	plant.setDistance(dto.getDistance());
    	plant.setFrost_resistance(dto.getFrost_resistance());
    	plant.setHygrometry(dto.getHygrometry());
    	plant.setIsEdible(dto.getIsEdible());
    	plant.setTaste_quality(dto.getTaste_quality());
    	plant.setVirtues(dto.getVirtues());
    	
    	plant.setAvoid(getSetFromLong(dto.getAvoid(), "plant"));
    	plant.setCohabit(getSetFromLong(dto.getCohabit(), "plant"));
    	
    	plant.setSoils(getSetFromLong(dto.getSoils(), "soil"));   	
    	plant.setWeathers(getSetFromLong(dto.getWeathers(), "weather"));
    	plant.setPests(getSetFromLong(dto.getPests(), "pest"));
    	plant.setTrim(getSetFromLong(dto.getTrim(), "period"));
    	plant.setSow(getSetFromLong(dto.getSow(), "period"));
    	plant.setFertilization(getSetFromLong(dto.getFertilization(), "period"));
    	plant.setHarvest(getSetFromLong(dto.getHarvest(), "period"));
    	plant.setPlantation(getSetFromLong(dto.getPlantation(), "period"));
    	
    	repository.save(plant);
    }
    
    public PlantSummary getSummary(Plant plant) {
    	PlantSummary summ = new PlantSummary();
    	
    	summ.setId(plant.getId());
    	summ.setName(plant.getName());
    	summ.setDescription(plant.getDescription());
    	summ.setPhotos(plant.getPhotos());
    	
    	summ.setAvoid(getAvoid(plant.getId()));
    	summ.setCohabit(getCohabit(plant.getId()));
    	
    	summ.setSunshine(plant.getSunshine());
    	summ.setWatering(plant.getWatering());
    	summ.setWidth(plant.getWidth());
    	summ.setHeight(plant.getHeight());
    	summ.setDistance(plant.getDistance());
    	summ.setFrost_resistance(plant.getFrost_resistance());
    	summ.setHygrometry(plant.getHygrometry());
    	summ.setIsEdible(plant.getIsEdible());
    	summ.setTaste_quality(plant.getTaste_quality());
    	summ.setVirtues(plant.getVirtues());
    	
    	summ.setFamily(plant.getFamily());
    	summ.setSoils(plant.getSoils());
    	summ.setWeathers(plant.getWeathers());
    	summ.setPests(plant.getPests());
    	summ.setTrim(plant.getTrim());
    	summ.setSow(plant.getSow());
    	summ.setFertilization(plant.getFertilization());
    	summ.setHarvest(plant.getHarvest());
    	summ.setPlantation(plant.getPlantation());
    	
    	return summ;
    }
    
    public Set<Long> getIds(Set objects, String type) {
		Iterator<Entity> iterator = objects.iterator();
    	Set<Long> list = new HashSet<Long>();
    	switch (type) {
		case "soil":
		    while ( iterator.hasNext() ) {
		    	list.add(((Soil) iterator.next()).getId());
		    }
			break;
		case "weather":
		    while ( iterator.hasNext() ) {
		    	list.add(((Weather) iterator.next()).getId());
		    }
			break;
		case "pest":
		    while ( iterator.hasNext() ) {
		    	list.add(((Pest) iterator.next()).getId());
		    }
			break;
		case "period":
		    while ( iterator.hasNext() ) {
		    	list.add(((Period) iterator.next()).getId());
		    }
			break;
	    case "plant":
		    while ( iterator.hasNext() ) {
		    	list.add(((Plant) iterator.next()).getId());
		    }
			break;
		case "photo":
		    while ( iterator.hasNext() ) {
		    	list.add(((Photo) iterator.next()).getId());
		    }
			break;
		default:
			list = null;
			break;
		}
    	return list;
    }
    
    public Set getSetFromLong(Set<Long> set, String type) {
    	Iterator<Long> iterator = set.iterator();
    	Set list = new HashSet<>();
    	switch (type) {
		case "soil":
		    while ( iterator.hasNext() ) {
		    	list.add(soilRep.findById(iterator.next()).get());
		    }
			break;
		case "weather":
		    while ( iterator.hasNext() ) {
		    	list.add(weatherRep.findById(iterator.next()).get());
		    }
			break;
		case "pest":
		    while ( iterator.hasNext() ) {
		    	list.add(pestRep.findById(iterator.next()).get());
		    }
			break;
		case "period":
		    while ( iterator.hasNext() ) {
		    	list.add(periodRep.findById(iterator.next()).get());
		    }
			break;
	    case "plant":
		    while ( iterator.hasNext() ) {
		    	list.add(repository.findById(iterator.next()).get());
		    }
			break;
		case "photo":
		    while ( iterator.hasNext() ) {
		    	list.add(photoRep.findById(iterator.next()).get());
		    }
			break;
		default:
			list = null;
			break;
	}
		return list;
    }
    
    public Set<Long> getAvoid(Long id) {
		return repository.getAvoid(id);
    }
    
    public Set<Long> getCohabit(Long id) {
		return repository.getCohabit(id);
    }
}
