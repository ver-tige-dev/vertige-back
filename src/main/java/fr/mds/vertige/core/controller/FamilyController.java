package fr.mds.vertige.core.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Category;
import fr.mds.vertige.core.model.Family;
import fr.mds.vertige.core.repository.FamilyRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/family")
public class FamilyController {
	private FamilyRepository repository;

    public FamilyController(FamilyRepository repository) {
        this.repository = repository;
    }

    @PostMapping
    public Family addFamily(@Validated @RequestBody Family family) {
        return repository.save(family);
    }

    @GetMapping
    public ResponseEntity<List<Family>> getAll() {
        List<Family> families = repository.findAll();
        return ResponseEntity.ok().body(families);
    }

    @GetMapping("/findId/{id}")
    public Optional<Family> findById(@PathVariable(value = "id") long id) {
        Optional<Family> family = repository.findById(id);
        return family;
    }
    
    @PutMapping("/edit/{id}")
    public void updateFamily(@PathVariable(value = "id") Long id, @Valid @RequestBody Family family) {
    	Family base = repository.findById(id).get();
    	base.setName(family.getName());
    	base.setDescription(family.getDescription());
    	
    	repository.save(base);
    }

    @DeleteMapping("/remove/{id}")
    public void removeById(@PathVariable(value = "id") Long id) {
        repository.deleteById(id);
    }

}
