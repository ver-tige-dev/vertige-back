package fr.mds.vertige.core.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.mds.vertige.core.model.Family;
import fr.mds.vertige.core.model.Photo;
import fr.mds.vertige.core.repository.PhotoRepository;

@RestController
@CrossOrigin
@RequestMapping("/api/photo")
public class PhotoController {
	private PhotoRepository repository;

    public PhotoController(PhotoRepository repository) {
        this.repository = repository;
    }

    @PostMapping("/add-photo")
    public Photo addPest(@Validated @RequestBody Photo photo) {
        return repository.save(photo);
        // ne devrais pas être utilisé, l'ajout de photo étant lié directement aux entités Plant, Pest, ...
    }

    @GetMapping
    public ResponseEntity<List<Photo>> getAll() {
        List<Photo> photos = repository.findAll();
        return ResponseEntity.ok().body(photos);
    }

    @GetMapping("/findId/{id}")
    public Optional<Photo> findById(@PathVariable(value = "id") long id) {
        Optional<Photo> photo = repository.findById(id);
        return photo;
    }
    
    @PutMapping("/edit/{id}")
    public void updatePhoto(@PathVariable(value = "id") Long id, @Valid @RequestBody Photo photo) {
    	Photo base = repository.findById(id).get();
    	base.setName(photo.getName());
    	base.setUrl(photo.getUrl());
    	
    	repository.save(base);
    }

    @DeleteMapping("/remove/{id}")
    public void removeById(@PathVariable(value = "id") Long id) {
        repository.deleteById(id);
    }

}
