#!/bin/sh

echo ">>> Genering War file ...";
sudo mvn clean install

cd docker

echo ">>> Build docker-compose file ...";
sudo docker-compose build

echo ">>> Run containers ...";
sudo docker-compose up -d

# echo ">>> Add datas to databases ...";
# docker stop $(docker ps -a -q)

# docker start vertige-back_mysql
# docker start vertige-back_pma
# docker start vertige-back_tomcat
# docker exec vertige-back_mysql 'mysql -u root -ptoor vertige </vertige.sql'
# docker exec -it <vertige-back_mysql> sh 'mysql -u root -p toor vertige </vertige.sql'
# docker exec -it <vertige-back_pma> sh 'mysql -u root -p toor vertige </vertige.sql'
